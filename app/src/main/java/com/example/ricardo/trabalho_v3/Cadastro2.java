package com.example.ricardo.trabalho_v3;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class Cadastro2 extends AppCompatActivity {

    private EditText telefone;
    private EditText logradouro;
    private EditText numero;
    private EditText complemento;
    private EditText bairro;
    private EditText cidade;
    private EditText estado;

    private Spinner nEscolaridade;

    private Button cadastro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro2);

        // link com os widgets da tela
        telefone = (EditText) findViewById(R.id.txt_telefone);
        logradouro = (EditText) findViewById(R.id.txt_logradouro);
        numero = (EditText) findViewById(R.id.txt_numeroEndereco);
        complemento = (EditText) findViewById(R.id.txt_complemento);
        bairro = (EditText) findViewById(R.id.txt_bairro);
        cidade = (EditText) findViewById(R.id.txt_cidade);
        estado = (EditText) findViewById(R.id.txt_estado);

        nEscolaridade = (Spinner) findViewById(R.id.sp_escolaridade);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.escolaridade, android.R.layout.simple_spinner_item);
        nEscolaridade.setAdapter(adapter);

        Button proximo = (Button) findViewById(R.id.bt_proximoCad2);

        //cria botão selection listener
        proximo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //String str_telefone = telefone.getText().toString();
                //String str_logradouro = logradouro.getText().toString();
                //String str_numero = numero.getText().toString();
                //String str_complemento = complemento.getText().toString();
                //String str_bairro = bairro.getText().toString();
                //String str_cidade = cidade.getText().toString();
                //String str_estado = estado.getText().toString();

                // dados capturados, podendo ser visualizados posteriormente
                //String item = nEscolaridade.getSelectedItem().toString();
                //Toast.makeText(getApplicationContext(), "Item escolhido: "+item, Toast.LENGTH_LONG).show();
                Intent acessarServicos = new Intent(Cadastro2.this,Servicos.class);
                startActivity(acessarServicos);
            }
        });
    }
}
