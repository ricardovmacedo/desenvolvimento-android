package com.example.ricardo.trabalho_v3;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MeusAnuncioss extends AppCompatActivity {

    private ListView meusAnuncioss;

    private Button criarAnuncio;
    private Button voltarMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meus_anuncioss);

        meusAnuncioss = (ListView)findViewById(R.id.lv_meusAnuncios);

        // Essa parte é fixada como exemplo
        ArrayList<String> anunciosDiv = preencherDados();

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, anunciosDiv);
        meusAnuncioss.setAdapter(arrayAdapter);
        // Até aqui (exemplo)

        criarAnuncio = (Button)findViewById(R.id.bt_criarAnuncio);
        criarAnuncio.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                Intent acessarServicos = new Intent(MeusAnuncioss.this, Anuncio.class);
                startActivity(acessarServicos);
            }
        });

        voltarMenu = (Button)findViewById(R.id.bt_menu);
        voltarMenu.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                Intent acessarMenu = new Intent(MeusAnuncioss.this, Servicos.class);
                startActivity(acessarMenu);
            }
        });
    }

    // Essa função é usada como exemplo - devendo ser substituída pelos dados reais (posteriormente)
    private ArrayList<String> preencherDados() {
        ArrayList<String> titulo = new ArrayList<String>();
        titulo.add("Aulas de Álgebar Linear");
        titulo.add("Aulas de Inglês");
        titulo.add("Aulas de Física (Eletromagnetismo)");
        titulo.add("Aulas de Química Orgânica");
        titulo.add("Aulas de Cálculo");
        titulo.add("Aulas de Francês");
        titulo.add("Aulas de Piano");
        titulo.add("Aulas de Teoria e Percepção Musical");

        return titulo;
    }
}
