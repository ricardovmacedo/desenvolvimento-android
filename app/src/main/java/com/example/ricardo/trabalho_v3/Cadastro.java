package com.example.ricardo.trabalho_v3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Cadastro extends AppCompatActivity {

    private Button proximoCad1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        proximoCad1 = (Button)findViewById(R.id.bt_proximoCad1);
        proximoCad1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                Intent acessarTelaInicial = new Intent(Cadastro.this,Cadastro2.class);
                startActivity(acessarTelaInicial);
            }
        });
    }
}
