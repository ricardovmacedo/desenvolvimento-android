package com.example.ricardo.trabalho_v3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class TelaInicial extends AppCompatActivity implements View.OnClickListener {
    private final int valida_id = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_inicial);

        Button entrar = (Button) findViewById(R.id.bt_entrar);
        entrar.setOnClickListener(this);

        Button cadastro = (Button) findViewById(R.id.bt_cadastro);
        cadastro.setOnClickListener(this);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == valida_id){
            if(resultCode == Activity.RESULT_OK){
                boolean acesso = data.getBooleanExtra("acesso",false);
                if(acesso == true){
                    Toast.makeText(this,"Bem Vindo " + data.getStringExtra("usuario"),Toast.LENGTH_SHORT).show();
                    Intent acessarMenu = new Intent(TelaInicial.this,Servicos.class);
                    startActivity(acessarMenu);
                }else {
                    Toast.makeText(this,"Email e/ou Senha Incorretos",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public void autenticar(View v){
        EditText email = (EditText)findViewById(R.id.txt_Email);
        EditText senha = (EditText)findViewById(R.id.txt_senha);

        Intent i = new Intent(this,ValidaLogin.class);
        i.putExtra("email",email.getText().toString());
        i.putExtra("senha",senha.getText().toString());
        startActivityForResult(i,valida_id);
    }
    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.bt_entrar:
                autenticar(view);
                break;
            case R.id.bt_cadastro:
                Intent acessarCadastro = new Intent(TelaInicial.this,Cadastro.class);
                startActivity(acessarCadastro);
                break;
            default:
                break;
        }
    }
}
