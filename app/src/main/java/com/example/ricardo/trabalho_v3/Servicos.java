package com.example.ricardo.trabalho_v3;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class Servicos extends AppCompatActivity {

    //private Button buscar;
    private Button buscar;
    private Button anunciar;
    private Button meusAnuncioss;
    private Button avaliar;
    private Button minhasAvaliacoes;
    private Button alterarDados;
    private Button logoff;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicos);

        buscar = (Button)findViewById(R.id.bt_buscar);
        buscar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                Intent acessarBusca = new Intent(Servicos.this,Buscar.class);
                startActivity(acessarBusca);
            }
        });

        anunciar = (Button)findViewById(R.id.bt_anunciar);
        anunciar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                Intent acessarTelaAnuncio = new Intent(Servicos.this,Anuncio.class);
                startActivity(acessarTelaAnuncio);
            }
        });

        meusAnuncioss = (Button)findViewById(R.id.bt_meusAnuncios);
        meusAnuncioss.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                Intent acessMeusAnunc = new Intent(Servicos.this, MeusAnuncioss.class);
                startActivity(acessMeusAnunc);
            }
        });

        avaliar = (Button)findViewById(R.id.bt_avaliar);
        avaliar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                Intent acessarAvaliar = new Intent(Servicos.this, Avaliar.class);
                startActivity(acessarAvaliar);
            }
        });

        minhasAvaliacoes = (Button)findViewById(R.id.bt_minhasAvaliacoes);
        minhasAvaliacoes.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                Intent acessarMinhasAvaliacoes = new Intent(Servicos.this, MinhasAvaliacoes.class);
                startActivity(acessarMinhasAvaliacoes);
            }
        });

        alterarDados = (Button)findViewById(R.id.bt_alterarDados);
        alterarDados.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                Intent acessarDadosCad = new Intent(Servicos.this, Cadastro.class);
                startActivity(acessarDadosCad);
            }
        });

        logoff = (Button)findViewById(R.id.bt_logoff);
        logoff.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                Intent acessarTelaInicial = new Intent(Servicos.this,TelaInicial.class);
                startActivity(acessarTelaInicial);
            }
        });
    }
}
