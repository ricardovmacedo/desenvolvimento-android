package com.example.ricardo.trabalho_v3;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MinhasAvaliacoes extends AppCompatActivity {

    private ListView minhasAvaliacoes;

    private Button voltarMenu2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_minhas_avaliacoes);

        minhasAvaliacoes = (ListView)findViewById(R.id.lv_minhasAvaliacoes);

        // Essa parte é fixada como exemplo
        ArrayList<String> avaliacoesDiv = preencherDados();

        ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, avaliacoesDiv);
        minhasAvaliacoes.setAdapter(arrayAdapter2);
        // Até aqui (exemplo)

        voltarMenu2 = (Button)findViewById(R.id.bt_menu2);
        voltarMenu2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                Intent acessarMenu2 = new Intent(MinhasAvaliacoes.this, Servicos.class);
                startActivity(acessarMenu2);
            }
        });
    }

    // Essa função é usada como exemplo - devendo ser substituída pelos dados reais (posteriormente)
    private ArrayList<String> preencherDados() {
        ArrayList<String> titulo = new ArrayList<String>();
        titulo.add("João - recomendo!");
        titulo.add("Maria - ótimas aulas!");
        titulo.add("José - boas aulas com preço acessível");
        titulo.add("Ana - boas aulas, porém um pouco caras...");
        titulo.add("Paula - ótimo trabalho!");
        titulo.add("Pedro - boas aulas, porém horário complicado...");
        titulo.add("Joaquina - super recomendo!");
        titulo.add("Felipe - bom professor!");

        return titulo;
    }
}
