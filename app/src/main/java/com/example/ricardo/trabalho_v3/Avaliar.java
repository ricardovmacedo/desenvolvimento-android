package com.example.ricardo.trabalho_v3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class Avaliar extends AppCompatActivity {

    private Spinner nNivelAulas2;
    private Spinner nDisciplinas2;

    Button postarAvaliacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avaliar);

        nNivelAulas2 = (Spinner)findViewById(R.id.sp_nivelAv);
        ArrayAdapter adapter4 = ArrayAdapter.createFromResource(this, R.array.nivelAula, android.R.layout.simple_spinner_item);
        nNivelAulas2.setAdapter(adapter4);

        nDisciplinas2 = (Spinner) findViewById(R.id.sp_DisciplinasAv);
        ArrayAdapter adapter5 = ArrayAdapter.createFromResource(this, R.array.disciplinas, android.R.layout.simple_spinner_item);
        nDisciplinas2.setAdapter(adapter5);

        postarAvaliacao = (Button)findViewById(R.id.bt_postarAvaliacao);
        postarAvaliacao.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                Intent postarAv = new Intent(Avaliar.this, Servicos.class);
                startActivity(postarAv);
            }
        });
    }
}
